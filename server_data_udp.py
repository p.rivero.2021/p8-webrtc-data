import asyncio
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription

async def consume_signaling(pc, addr, receive_udp_message, send_udp_message=None):
    while True:
        obj = await receive_udp_message()

        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                # Respuesta de envío
                await pc.setLocalDescription(await pc.createAnswer())
                await send_udp_message(pc.localDescription, addr)
        elif obj == "BYE":
            print("Exiting")
            break

async def run_answer(pc, addr):
    async def receive_udp_msg():
        # Lógica de recepción UDP
        return pc
    await consume_signaling(pc, addr, receive_udp_message=receive_udp_msg)
    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):
            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

    await consume_signaling(pc, addr, receive_udp_message=None)

if __name__ == "__main__":
    asyncio.run(run_answer(RTCPeerConnection(), ("127.0.0.1", 12345)))
