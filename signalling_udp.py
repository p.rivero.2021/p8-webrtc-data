import asyncio
import json

class UDPSignalingProtocol(asyncio.DatagramProtocol):
    def __init__(self, on_receive, registered_clients, registered_servers):
        self.on_receive = on_receive
        self.registered_clients = registered_clients
        self.registered_servers = registered_servers

    def datagram_received(self, data, addr):
        message = data.decode()
        self.on_receive(message, addr)

async def handle_register_message(message, addr, registered_clients, registered_servers):
    if message == "REGISTER CLIENT":
        registered_clients.add(addr)
        print(f"Registered client: {addr}")
    elif message == "REGISTER SERVER":
        registered_servers.add(addr)
        print(f"Registered server: {addr}")

async def main():
    loop = asyncio.get_running_loop()

    registered_clients = set()
    registered_servers = set()

    async def on_receive(message, addr):
        print(f"Received from {addr}: {message}")
        await handle_register_message(message, addr, registered_clients, registered_servers)

    udp_signaling = UDPSignalingProtocol(on_receive, registered_clients, registered_servers)

    transport, _ = await loop.create_datagram_endpoint(
        lambda: udp_signaling,
        local_addr=('127.0.0.1', 9999)  # Cambia la dirección y el puerto según tu configuración del servidor UDP
    )

    try:
        await asyncio.Future()  # Espera indefinida para que el servidor siga ejecutándose
    except KeyboardInterrupt:
        pass
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())
