import asyncio
import time
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription

async def consume_signaling(pc, addr, receive_udp_message=None, send_udp_message=None):
    while True:
        obj = await receive_udp_message()

        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                # Respuesta de envío
                await pc.setLocalDescription(await pc.createAnswer())
                await send_udp_message(pc.localDescription, addr)
        elif obj == "BYE":
            print("Exiting")
            break

async def run_offer(pc, addr, send_udp_message):
    channel = pc.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")

    async def send_udp(pc, addr):

        # Lógica de envío UDP

        await send_udp_message(pc.localDescription, addr, send_udp_message=send_udp)
        # asyncio.run(run_offer(RTCPeerConnection(), ("127.0.0.1", 9999), send_udp))

    async def send_pings():
        while True:
            message = f"ping {int(time.time() * 1000)}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")

        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (int(time.time() * 1000) - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    # send offer
    await pc.setLocalDescription(await pc.createOffer())
    await send_udp_message(pc.localDescription, addr)

    await consume_signaling(pc, addr)

if __name__ == "__main__":
    asyncio.run(run_offer(RTCPeerConnection(), ("127.0.0.1", 9999), send_udp_message))
